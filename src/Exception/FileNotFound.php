<?php

namespace Selene\ConfigProvider\Exception;

class FileNotFound extends \Exception
{
    protected $message = 'File %s not found';

    public function __construct($filename = '', $message = null)
    {
        if (!$message) {
            $message = sprintf($this->message, $filename);
        }

        parent::__construct($message, 500);
    }
}

